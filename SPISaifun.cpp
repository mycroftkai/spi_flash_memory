#include "SPISaifun.h"

SPISaifun::SPISaifun(byte cs) {
  this->cs = cs;
  settings = SPISettings(4000000, MSBFIRST, SPI_MODE0);
}

void SPISaifun::init() {
  pinMode(cs, OUTPUT);
  SPI.begin();
  enableWriting();
}

int SPISaifun::getDeviceId() {
  beginChip();
  int deviceId = sendCommand(SPI_DEVICE_ID, 5);
  endChip();
  return deviceId;
}

void SPISaifun::eraseBytes() {
  enableWriting();
  beginChip();
  SPI.transfer(SPI_ERASE);
  endChip();
  while (readStatus() != 0);
}

int SPISaifun::readStatus() {
  beginChip();
  SPI.transfer(SPI_STATUS);
  int status = SPI.transfer(0);
  endChip();
  return status;
}

void SPISaifun::writeBytes(int address, byte buffer[], int sizeBuffer) {
  int lastAddress = address + sizeBuffer;
  selectPage(address);

  for (int i = address; i < lastAddress; i++) {
    SPI.transfer(buffer[i - address]);
    if (i % 256 == 0) selectPage(i);
  }

  endChip();
}

void SPISaifun::selectPage(int address) {
  endChip();
  enableWriting();
  beginChip();
  SPI.transfer(SPI_WRITE_BYTE);
  SPI.transfer(0);
  SPI.transfer(getNumberPage(address));
  SPI.transfer(address);
}

void SPISaifun::writeByte(int start, byte data) {
  enableWriting();
  beginChip();
  SPI.transfer(SPI_WRITE_BYTE);
  SPI.transfer(0);
  SPI.transfer(getNumberPage(start));
  SPI.transfer(start);
  SPI.transfer(data);
  endChip();
}

byte* SPISaifun::readBytes(int start, int count) {
  byte* buffer = new byte[count];
  beginChip();
  SPI.transfer(SPI_READ_BYTES);
  SPI.transfer(0);
  SPI.transfer(getNumberPage(start));
  SPI.transfer(start);
  SPI.transfer(0);
  for (int i = 0; i < count; i++) buffer[i] = SPI.transfer(0);
  endChip();

  return buffer;
}

byte SPISaifun::readByte(int start) {
  beginChip();
  SPI.transfer(SPI_READ_BYTE_FREQ);
  SPI.transfer(start >> 16);
  SPI.transfer(start >> 8);
  SPI.transfer(start);
  byte result = SPI.transfer(0);

  endChip();
  return result;
}

void SPISaifun::enableWriting() {
  beginChip();
  SPI.transfer(SPI_WRITABLE);
  endChip();
}

void SPISaifun::beginChip() {
  SPI.beginTransaction(settings);
  digitalWrite(cs, LOW);
}

void SPISaifun::endChip() {
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
}

int SPISaifun::readContainer(int numberContainer) {
  int result = 0;
  for (int i = 0; i < numberContainer; i++) result = SPI.transfer(result);
  return result;
}

int SPISaifun::sendCommand(int commandName, int numberContainer) {
  SPI.transfer(commandName);
  return readContainer(numberContainer);
}

int SPISaifun::getNumberPage(int start) {
  return start / 256;
}
