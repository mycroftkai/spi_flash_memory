#include <SPI.h>
#include <Arduino.h>

#define PAGE_SIZE 256

class SPISaifun {

  public:
    byte SPI_DEVICE_ID =        0xAB;
    byte SPI_WRITABLE =         0x06;
    byte SPI_READ_BYTES =       0x0B;
    byte SPI_READ_BYTE_FREQ =   0x03;
    byte SPI_WRITE_BYTE =       0x02;
    byte SPI_STATUS =           0x05;
    byte SPI_ERASE =            0xC7;

    byte cs;

    SPISaifun(byte cs);

    void init();
    void eraseBytes();
    void writeBytes(int start, byte* buffer, int sizeBuffer);
    void writeByte(int start, byte data);

    byte* readBytes(int address, int count);
    byte readByte(int start);

    int getDeviceId();
    int readStatus();

  private:
    SPISettings settings;

    void enableWriting();
    void beginChip();
    void endChip();
    void selectPage(int address);

    int readContainer(int count);
    int sendCommand(int commandName, int numberContainer);
    int getNumberPage(int start);
};
