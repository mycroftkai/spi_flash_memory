#include "SPISaifun.h"

#define CS_PIN PA3

SPISaifun flash(CS_PIN);

void setup() {
  Serial.begin(9600);

  //Если комманды не сходятся, их можно заменить.
  //Поддерживаемые команды чипа можно найти в даташите к чипу.
  //
  //flash.SPI_DEVICE_ID =        0xAB;
  //flash.SPI_WRITABLE =         0x06;
  //flash.SPI_READ_BYTES =       0x0B;
  //flash.SPI_READ_BYTE_FREQ =   0x03;
  //flash.SPI_WRITE_BYTE =       0x02;
  //flash.SPI_STATUS =           0x05;
  //flash.SPI_ERASE =            0xC7;

  flash.init();
}

void loop() {
  if (Serial.available()) {
    char input = Serial.read();

    switch (input) {
      case 'r': readBytes(); break;
      case 'w': writeBytes(); break;
      case 'e': eraseBytes(); break;
      case 'd': deviceId(); break;
    }
  }
}

void readBytes() {
  Serial.println("Reading 512 bytes");

  byte* buffer = flash.readBytes(0, 512);

  for (int i = 0; i < 512; i++) {
    Serial.print(buffer[i], HEX);
    if (i % 30 == 0) Serial.println();
  }

  Serial.println();
  Serial.println(String((char*)buffer));
  Serial.println();
}

void writeBytes() {
  String text = "Hello World";
  Serial.print("Write string: ");
  Serial.println(text);
  byte buffer[text.length() + 1];
  text.getBytes(buffer, text.length() + 1);
  flash.writeBytes(254, buffer, text.length() + 1);
}

void eraseBytes() {
  Serial.print("Erasing....");
  flash.eraseBytes();
  Serial.println("Done");
}

void deviceId() {
  Serial.print("device id: ");
  Serial.println(flash.getDeviceId());
}
